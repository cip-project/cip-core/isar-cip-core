#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2023
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: MIT
#

inherit dpkg-raw

FILESEXTRAPATHS:prepend := "${FILE_DIRNAME}/files:"

DPKG_ARCH = "all"
PROVIDES += "swupdate-certificates"
DEBIAN_PROVIDES = "swupdate-certificates"

SWU_SIGN_CERT ??= ""

SRC_URI:append = " ${@ "file://"+d.getVar('SWU_SIGN_CERT') if d.getVar('SWU_SIGN_CERT') else '' }"

do_install() {
    if [ -z ${SWU_SIGN_CERT} ]; then
        bbfatal "You must set SWU_SIGN_CERT and provide the required file as artifacts to this recipe"
    fi
    TARGET=${D}/usr/share/swupdate-signing/
    install -d -m 0700 ${TARGET}
    install -m 0700 ${WORKDIR}/${SWU_SIGN_CERT} ${TARGET}/swupdate-sign.crt
}

