#!/bin/sh
#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2023-2024
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: MIT


open_tpm2_partition() {
	partition_device="$1"
	crypt_mount_name="$2"
	tpm_device="$3"
	if ! TPM2TOOLS_TCTI="device:$tpm_device" usr/bin/clevis luks unlock -n "$crypt_mount_name" \
		 -d "$partition_device"; then
		panic "Can't decrypt '$partition_device' !"
	fi
}

enroll_tpm2_token() {
	partition_device="$1"
	passphrase="$2"
	tpm_device="$3"
	tpm_key_algorithm="$4"
	pcr_bank_hash_type="$5"
	if [ -x /usr/bin/clevis ]; then
		TPM2TOOLS_TCTI="device:$tpm_device" clevis luks bind -d "$partition_device" tpm2 '{"key":"'"$tpm_key_algorithm"'", "pcr_bank":"'"$pcr_bank_hash_type"'","pcr_ids":"7"}' < "$passphrase"
	else
		panic "clevis not available cannot enroll tpm2 key!"
	fi
}

prepare_for_encryption() {
	# clevis needs /dev/fd create it in the initramfs
	if [ ! -e /dev/fd ]; then
		ln -s /proc/self/fd /dev/fd
	fi
}

finalize_tpm2_encryption() {
	partition_device="$1"
	cryptsetup -v luksKillSlot -q  "$partition_device" 0
}
